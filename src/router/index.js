import Vue from 'vue';
import VueRouter from 'vue-router';
import * as routes from './routes';
import Home from '../views/Home';
import Open from '../views/Issues/Open';
import Done from '../views/Issues/Done';
import Trashed from '../views/Issues/Trashed';

Vue.use(VueRouter);

export default new VueRouter({
	mode: `history`,

	routes: [
		{
			path: routes.home,
			component: Home,
		},

		{
			path: routes.issuesOpen,
			component: Open,
		},

		{
			path: routes.issuesDone,
			component: Done,
		},

		{
			path: routes.issuesTrashed,
			component: Trashed,
		},
	],
});
