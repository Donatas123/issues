export const home = `/`;
export const issuesOpen = `/issues/open`;
export const issuesDone = `/issues/done`;
export const issuesTrashed = `/issues/trashed`;
