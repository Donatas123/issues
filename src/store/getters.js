export default {
	openIssues(state) {
		return state.issues.filter((issue) => {
			return !issue.isDone && !issue.isTrashed;
		});
	},

	doneIssues(state) {
		return state.issues.filter((issue) => {
			return issue.isDone && !issue.isTrashed;
		});
	},

	trashedIssues(state) {
		return state.issues.filter((issue) => {
			return issue.isTrashed;
		});
	},
};
