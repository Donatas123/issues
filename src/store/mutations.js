export default {
	addIssue(state, issueText) {
		state.issues.push({
			isTrashed: false,
			isDone: false,
			text: issueText,
			createdAt: +new Date(),
		});
	},

	updateIssueText(state, {issueCreatedAt, newText}) {
		const targetIssue = state.issues.find((issue) => {
			return issue.createdAt === issueCreatedAt;
		});

		targetIssue.text = newText;
	},

	toggleIssueIsDoneState(state, issueCreatedAt) {
		const targetIssue = state.issues.find((issue) => {
			return issue.createdAt === issueCreatedAt;
		});

		targetIssue.isDone = !targetIssue.isDone;
	},

	toggleIssueIsTrashedState(state, issueCreatedAt) {
		const targetIssue = state.issues.find((issue) => {
			return issue.createdAt === issueCreatedAt;
		});

		targetIssue.isTrashed = !targetIssue.isTrashed;
	},
};
